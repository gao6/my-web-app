FROM php:7.4-apache
USER root

# Set the ServerName directive in Apache configuration
#RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

RUN echo "Listen 8080" >/etc/apache2/ports.conf
RUN echo "======== ports.conf"
RUN cat /etc/apache2/ports.conf
RUN echo "======== apache2.conf"
RUN cat /etc/apache2/apache2.conf

# Copy your index.php to the Apache document root
COPY index.php /var/www/html/

# Change ownership of the copied file to the non-root user (replace 'www-data' with your non-root user if different)
RUN chown -R www-data:www-data /var/www/html/

# Expose the port on which Apache is running inside the container (e.g., 8080)
EXPOSE 8080